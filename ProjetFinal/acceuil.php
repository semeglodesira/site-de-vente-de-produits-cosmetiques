<html>
    <head>
        <title> Produits coestimétiques </title>
        <style>
            li{
                display:inline;
                margin-right:100px;
            }
            a{
                color:black;
            }
            img{
                width:100%;
                height:60%;
            }
        </style>
    </head>
    <body>
        <div align="center">
        <ul>
        
            <li>
                <a href="acceuil.php"> Acceuil </a>
            </li>
            <li>
                <a href="login.php"> Login </a>
            </li>
            <li>
                <a href="contact.php"> Contact </a>
            </li>
            <li>
                <a href="about.php"> About us </a>
            </li>
           
        </ul>
        </div>
        <div>
            <img src="./images/img1.jpg"/>
            <h1 align="center"> Découvrer nos produits Divine Essence </h1> 
            <p> Bienvenue dans notres site de vente des produits coesmétiques en ligne </p>
            <p> Toute substance qu'une personne applique sur sa peau, ses cheveux, ses ongles ou ses dents à des fins de nettoyage ou dans le but d'améliorer ou de modifier son apparence est un « cosmétique ». Les produits de beauté (parfums, crèmes pour la peau, vernis à ongles et maquillage) et les produits de toilette (savons, shampoings, crèmes à raser et désodorisants) sont aussi des cosmétiques. </p>
            <p> Un produit destiné aux soins personnels se définit comme une substance ou un mélange de substances, qui est généralement reconnu par le public comme un produit de nettoyage ou aux soins quotidiens. Les ingrédients et les allégations servent de critères pour déterminer qu'un tel produit est un cosmétique ou un médicament.

Un produit de beauté ou de toilette est habituellement classé dans la catégorie des cosmétiques. Cependant, si on lui attribue le pouvoir de modifier les fonctions de l'organisme, ou de prévenir ou de traiter une maladie, il est considéré comme un médicament ou un produit de santé naturel au sens de la loi. Un tel produit affichera un numéro d'identification de médicament (DIN) ou un numéro de produit naturel (NPN) sur son étiquette. </p>
        </div>
    </body>
</html>