-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 17 mars 2022 à 02:06
-- Version du serveur : 10.4.22-MariaDB
-- Version de PHP : 8.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `projetfinal`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `login` varchar(10) NOT NULL,
  `password` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`login`, `password`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `Nom` varchar(15) NOT NULL,
  `Prenom` varchar(15) NOT NULL,
  `Telephone` varchar(20) NOT NULL,
  `Couriel` varchar(30) NOT NULL,
  `Date de naissance` varchar(20) NOT NULL,
  `Adresse` varchar(20) NOT NULL,
  `Sexe` varchar(20) NOT NULL,
  `Login` varchar(20) NOT NULL,
  `Password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`Nom`, `Prenom`, `Telephone`, `Couriel`, `Date de naissance`, `Adresse`, `Sexe`, `Login`, `Password`) VALUES
('Chatir', 'Maroua', '5148704587', 'user@gmail.com', '08-05-2002', '1234 Rue Sylvain', 'F', 'user', 'user'),
('Daisy', 'Desira', '438-345-9809', 'daisy@gmil.com', '1999-03-14', '4125 Rue Parthenais', 'F', 'daisy', 'daisy'),
('Hamza', 'Deossi', '4389094553', 'hamza@gmail.com', '09-11-2003', '3040 rue parthenais', 'M', 'utilisateur', 'utilisateur');

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `id_produit` int(3) NOT NULL,
  `reference` varchar(20) NOT NULL,
  `categorie` varchar(40) NOT NULL,
  `titre` varchar(30) NOT NULL,
  `description` text NOT NULL,
  `public` varchar(40) NOT NULL,
  `photo` varchar(250) NOT NULL,
  `prix` int(3) NOT NULL,
  `stock` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`id_produit`, `reference`, `categorie`, `titre`, `description`, `public`, `photo`, `prix`, `stock`) VALUES
(1, '11-d-23', 'huile', 'Huile Amande douce', 'Pour les cheveux et la peau', 'mixte', 'Photos/11-d-23_amande.jpg', 20, 53),
(2, '66-f-15', 'huile', 'Huile Argan', 'Anti-douleur et Anti-Inflamtoir', 'mixte', 'Photos/66-f-15_argan.jpg', 15, 230),
(3, '88-g-77', 'huile', 'Huile ARnica', 'Coissance des cheuveux', 'mixte', 'Photos/88-g-77_arnica.jpg', 20, 40),
(4, '31-p-33', 'huile', 'Huile Basilic', 'Remède intestin', 'mixte', 'Photos/31-p-33_basilic.jpg', 25, 3),
(5, '55-b-38', 'huile', 'Huile Avocat', 'Nourrissante et reparatrice', 'mixte', 'Photos/55-b-38_avocat.jpg', 20, 80),
(6, '63-s-63', 'huile', 'Eau Bleuet', 'Tout type de peau', 'mixte', 'Photos/63-s-63_blueut.jpeg', 59, 120),
(7, '77-p-79', 'Huile', 'Cannelle', 'Rhume et grippe', 'mixte', 'Photos/77-p-79_cannelle.jpg', 79, 99);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`login`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`Nom`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`id_produit`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
  MODIFY `id_produit` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
